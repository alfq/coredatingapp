import { User } from "./_models/User";
import { AuthService } from "./_services/auth.service";
import { Component, OnInit } from "@angular/core";
import { JwtHelperService } from "@auth0/angular-jwt";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  constructor(
    private jwtHelperService: JwtHelperService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    const token = localStorage.getItem("token");
    const user: User = JSON.parse(localStorage.getItem("user"));
    if (token)
      this.authService.decodedToken = this.jwtHelperService.decodeToken(token);

    if (user) {
      this.authService.currentUser = user;
      if (this.authService.currentUser.photoUrl != null)
        this.authService.changeMemeberPhoto(user.photoUrl);
      else this.authService.changeMemeberPhoto("../assets/user.png");
    }
  }
}
